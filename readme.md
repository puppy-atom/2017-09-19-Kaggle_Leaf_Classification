# *Kaggle Leaf Classification Competition* presented by Nick Buker

ATOM is meeting on Tuesday, 19th September, 6:30pm, at Galvanize!  

Join us for a highly participative discussion ! The subject of discussion is the [Kaggle Leaf Classification competition](https://www.kaggle.com/c/leaf-classification). Please read up a little to prepare for the discussion. The winning [blog post](http://blog.kaggle.com/2017/03/24/leaf-classification-competition-1st-place-winners-interview-ivan-sosnovik/) is a great place to start; otherwise, [this article](https://www.researchgate.net/profile/Charles_Mallah/publication/266632357_Plant_Leaf_Classification_using_Probabilistic_Integration_of_Shape_Texture_and_Margin_Features/links/547072f40cf216f8cfa9f72c.pdf) about the features used by the winners could be interesting. The discussion will be led by our coorganizer, Nick Buker !  

Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.  

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !  

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.  

We're kindly hosted by Galvanize. Thank you !  
